﻿using System;
using System.Collections.Generic;
using AppSDK;

namespace App
{
    class Program
    {
        static void Main(string[] args)
        {
            PluginManager.LoadPlugin(@"C:\Users\Administrator\Documents\Visual Studio 2015\Projects\AppSDK\SQLServerClient_plugin\bin\Debug\SQLServerClient_plugin.dll");
            Dictionary<string, IPlugin> plugins = PluginManager.listPlugins();
            foreach (KeyValuePair<string, IPlugin> p in plugins)
            {
                Console.WriteLine(p.Key);
                p.Value.run();
            }
            Console.ReadKey();
        }
    }
}
