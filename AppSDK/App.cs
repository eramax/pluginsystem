﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace AppSDK
{
    public class Loger : ILoger
    {
        public void log(string message)
        {
            Console.WriteLine("Loger: " + message);
        }
    }
    public class Config : IConfig
    {
        public ILoger getLoger()
        {
            return new Loger();
        }

        public IUserManager getUserManager()
        {
            throw new NotImplementedException();
        }

        public string getDbConnectionString()
        {
            return @"Data Source=.\SQLEXPRESS;Initial Catalog=AppDb;Integrated Security=True;";
        }
    }
    public static class PluginManager
    {
        private static Dictionary<string, IPlugin> _plugins = new Dictionary<string, IPlugin>();
        private static Config _config = new Config();
        public static bool LoadPlugin(string dllFile)
        {
            try
            {
                Assembly.LoadFrom(dllFile);
                foreach(Assembly a in AppDomain.CurrentDomain.GetAssemblies())
                {
                    foreach(Type t in a.GetTypes())
                    {
                        if(t.GetInterface("IPlugin") != null)
                        {
                            IPlugin plugin = Activator.CreateInstance(t) as IPlugin;
                            _plugins.Add(plugin.pluginName(), plugin);
                            _plugins[plugin.pluginName()].load(_config);
                        }
                    }
                }
            }
            catch (Exception ex) { return false; }
            return true;
        }
        public static Dictionary<string, IPlugin> listPlugins()
        {
            return _plugins;
        }
    }
}
