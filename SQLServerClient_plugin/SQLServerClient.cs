﻿using System;
using AppSDK;
using System.Data.SqlClient;

namespace SQLServerClient_plugin
{
    public class SQLServerClient : IPlugin
    {
        private ILoger _logger;
        private string _connectionString;
        private IUserManager _userManager;

        public void load(IConfig conf)
        {
            _logger = conf.getLoger();
            _connectionString = conf.getDbConnectionString();
        }
        public void setConnectionString(string connectionString)
        {
            _connectionString = connectionString;
        }

        public string pluginName()
        {
            return "SQLServerClient";
        }
           
        public void run()
        {
            connect(_connectionString);
        }
        public void connect(string connectionString)
        {
            try
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                Console.Write("Connecting to SQL Server ... ");
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    Console.WriteLine("Done.");
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }

            Console.WriteLine("All done. Press any key to finish...");
            Console.ReadKey(true);
        }

        public void unload()
        {
            
        }
    }
}
